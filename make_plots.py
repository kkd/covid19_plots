"""
This script attempts to replicate the log-scale plots of the Covid-19
pandemic as seen in the Financial Times and other sources.  The plots
show each locale's trajectory starting from when a threshold value is
reached (100 confirmed cases or 10 deaths).  Each plot contains up to
20 locales, taking those whose latest tally is highest.

The dataset of locales, their labels, and their timelines of confirmed
cases and deaths are sourced from the [Johns Hopkins data
repo](https://github.com/CSSEGISandData/COVID-19).  Regional groupings
are sourced from the [WHO website](https://who.int), particularly
their [Covid-19 situation
reports](https://www.who.int/emergencies/diseases/novel-coronavirus-2019/situation-reports).
The author makes no claim regarding the structure, completeness, or
accuracy of the source data.

Currently, this script generates plots for:

 * the world, by 'Country/Region' in the JH dataset

 * each WHO region, by 'Country/Region' in the JH dataset.  Note that
   due to overseas territories, some entries may appear in multiple
   WHO regions.

 * the United States, by state


To run the script:

    # Downloads latest data and plots it
    python make_plots.py all

    # Downloads only
    python make_plots.py download

    # Plots only (using previously downloaded data)
    python make_plots.py plot

The script requires the following python modules:

 * [matplotlib](https://matplotlib.org)

 * [pandas](https://pandas.pydata.org)

 * [plac](https://micheles.github.io/plac/)

 * [requests](https://requests.readthedocs.io)

Copyright 2020 K.Dorn

Permission is hereby granted, free of charge, to any person obtaining
a copy of this software and associated documentation files (the
"Software"), to deal in the Software without restriction, including
without limitation the rights to use, copy, modify, merge, publish,
distribute, sublicense, and/or sell copies of the Software, and to
permit persons to whom the Software is furnished to do so, subject to
the following conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
"""

import io
import math
import os
import shutil
import typing
import zipfile

import matplotlib.pyplot as plt
import pandas as pd
import requests

class DataFilePaths( typing.NamedTuple ):
    confirmed_path: str
    deaths_path:    str

DEFAULT_ARCHIVE_URL = 'https://github.com/CSSEGISandData/COVID-19/archive/master.zip'

DEFAULT_GLOBAL_FILE_PATHS = DataFilePaths( confirmed_path = 'COVID-19-master/csse_covid_19_data/csse_covid_19_time_series/time_series_covid19_confirmed_global.csv',
                                           deaths_path    = 'COVID-19-master/csse_covid_19_data/csse_covid_19_time_series/time_series_covid19_deaths_global.csv')

DEFAULT_US_FILE_PATHS = DataFilePaths( confirmed_path = 'COVID-19-master/csse_covid_19_data/csse_covid_19_time_series/time_series_covid19_confirmed_US.csv',
                                       deaths_path    = 'COVID-19-master/csse_covid_19_data/csse_covid_19_time_series/time_series_covid19_deaths_US.csv')

class ColumnNames( typing.NamedTuple ):
    region_column:   str
    country_column:  str
    province_column: str

DEFAULT_GLOBAL_COLUMN_NAMES = ColumnNames( region_column   = 'WHO_Region',
                                           country_column  = 'Country/Region',
                                           province_column = 'Province/State' )

DEFAULT_US_COLUMN_NAMES = ColumnNames( region_column   = 'WHO_Region',
                                       country_column  = 'Country_Region',
                                       province_column = 'Province_State' )

# WHO regions, taken from
# https://www.who.int/emergencies/diseases/novel-coronavirus-2019/situation-reports
WORLD_REGIONS = { 'Africa': ('Algeria',
                             'Angola',
                             'Benin',
                             'Botswana',
                             'Burkina Faso',
                             'Burundi',
                             'Cameroon',
                             'Cabo Verde',
                             'Cape Verde',
                             'Central African Republic',
                             'Chad',
                             'Comoros',
                             'Congo (Brazzaville)',
                             'Congo (Kinshasa)',
                             'Cote d\'Ivoire',
                             'Ivory Coast',
                             'Democratic Republic of the Congo',
                             'Equatorial Guinea',
                             'Eritrea',
                             'Eswatini',
                             'Ethiopia',
                             'Gabon', 'Gambia', 'Ghana',
                             'Guinea', 'Guinea-Bissau',
                             'Kenya',
                             'Lesotho',
                             'Liberia',
                             'Madagascar', 'Malawi', 'Mali',
                             'Mauritania', 'Mauritius', 'Mayotte', 'Mozambique',
                             'Namibia', 'Niger', 'Nigeria',
                             'Republic of the Congo',
                             'Reunion',
                             'Rwanda',
                             'São Tomé and Príncipe', 'Senegal', 'Seychelles',
                             'Sierra Leone', 'South Africa', 'South Sudan', 'Swaziland',
                             'Togo',
                             'Uganda',
                             'Tanzania',
                             'Western Sahara',
                             'Zambia',
                             'Zimbabwe' ),
                  'Americas': ('Anguilla',
                               'Antigua and Barbuda', 'Argentina', 'Aruba',
                               'Bahamas', 'Barbados',
                               'Belize', 'Bermuda',
                               'Bolivia', 'Bonaire, Sint Eustatius and Saba', 'Brazil',
                               'British Virgin Islands',
                               'Canada', 'Cayman Islands', 'Chile',
                               'Colombia', 'Costa Rica', 'Cuba', 'Curacao',
                               'Dominica', 'Dominican Republic',
                               'Ecuador', 'El Salvador',
                               'Falkland Islands (Islas Malvinas)',
                               'French Guiana',
                               'Grenada', 'Guadeloupe', 'Guatemala', 'Guyana',
                               'Haiti', 'Honduras',
                               'Jamaica',
                               'Martinique',
                               'Mexico',
                               'Montserrat',
                               'Nicaragua',
                               'Panama', 'Paraguay', 'Peru',
                               'Saint Barthelemy',
                               'Saint Kitts and Nevis', 'Saint Lucia',
                               'Saint Vincent and the Grenadines', 'Sint Maarten', 'St Martin',
                               'Suriname',
                               'Trinidad and Tobago',
                               'Turks and Caicos Islands',
                               'US', 'United States',
                               'Uruguay',
                               'Venezuela'),
                  'Southeast Asia': ('Bangladesh',
                                     'Bhutan',
                                     'Burma',
                                     'North Korea',
                                     'India',
                                     'Indonesia',
                                     'Maldives',
                                     'Myanmar',
                                     'Nepal',
                                     'Sri Lanka',
                                     'Thailand',
                                     'Timor-Leste',
                                     'Pakistan'),
                  'Europe': ('Albania', 'Andorra', 'Armenia', 'Austria', 'Azerbaijan',
                             'Belarus', 'Belgium', 'Bosnia and Herzegovina', 'Bulgaria',
                             'Channel Islands', 'Croatia',
                             'Cyprus', 'Czech Republic', 'Czechia',
                             'Denmark',
                             'Estonia',
                             'Faroe Islands', 'Finland', 'France',
                             'Georgia', 'Germany', 'Gibraltar', 'Greece', 'Greenland',
                             'Holy See', 'Hungary',
                             'Iceland', 'Ireland', 'Isle of Man', 'Israel', 'Italy',
                             'Kazakhstan', 'Kosovo', 'Kyrgyzstan',
                             'Latvia', 'Liechtenstein', 'Lithuania', 'Luxembourg',
                             'Malta', 'Monaco', 'Montenegro',
                             'Netherlands', 'North Macedonia', 'Norway',
                             'Poland', 'Portugal',
                             'Moldova',
                             'Romania', 'Russia',
                             'San Marino', 'Serbia', 'Slovakia',
                             'Slovenia', 'Spain', 'Sweden', 'Switzerland',
                             'Tajikistan', 'Turkey', 'Turkmenistan',
                             'Ukraine', 'United Kingdom', 'Uzbekistan'),
                  'Eastern Mediterranean': ('Afghanistan',
                                            'Bahrain',
                                            'Djibouti',
                                            'Egypt',
                                            'Iran', 'Iraq',
                                            'Jordan',
                                            'Kuwait',
                                            'Lebanon',
                                            'Libya',
                                            'Morocco',
                                            'Oman',
                                            'Pakistan',
                                            'Qatar',
                                            'Saudi Arabia', 'Somalia', 'Sudan', 'Syria',
                                            'Tunisia',
                                            'United Arab Emirates',
                                            'West Bank and Gaza',
                                            'Yemen'),
                  'Western Pacific': ('Australia',
                                      'Brunei',
                                      'Cambodia', 'China',
                                      'Cook Islands',
                                      'Fiji', 'French Polynesia',
                                      'Japan',
                                      'Kiribati', 'Korea, South',
                                      'Laos',
                                      'Malaysia', 'Marshall Islands', 'Micronesia', 'Mongolia',
                                      'Nauru', 'New Caledonia', 'New Zealand', 'Niue',
                                      'Palau', 'Papua New Guinea', 'Philippines',
                                      'South Korea', 'Samoa', 'Singapore', 'Solomon Islands',
                                      'Taiwan', 'Taiwan*', 'Tonga', 'Tuvalu',
                                      'Vanuatu', 'Vietnam')}

PLOT_DIR='plots'

def download( archive_url: str           = DEFAULT_ARCHIVE_URL,
              paths:       DataFilePaths = DEFAULT_GLOBAL_FILE_PATHS ) -> DataFilePaths:

    ret = requests.get( archive_url )
    # TODO:  error checking
    b = io.BytesIO( ret.content )
    f = zipfile.ZipFile( b )
    cp = f.extract( paths.confirmed_path )
    dp = f.extract( paths.deaths_path )
    return DataFilePaths( cp, dp )

def to_filename( s ):
    return s.replace(',', '').replace(' ', '_').replace('/', '')

TimeseriesCollection = typing.Dict[ str, typing.Sequence[int] ]

class PlotDescription( typing.NamedTuple ):

    plot_type:  str
    name:       str
    last_date:  str
    series:     TimeseriesCollection
    threshold:  int

def tc_max_val( series: TimeseriesCollection ):

    if not series:
        return 0

    return max( [ max(vs) for _,vs in series.items() ] )

def tc_max_len( series: TimeseriesCollection ):

    if not series:
        return 0

    return max( [ len(vs) for _,vs in series.items() ] )

def describe_country_plots( paths: DataFilePaths,
                            column_names: ColumnNames ):

    df_c = pd.read_csv( paths.confirmed_path )
    df_d = pd.read_csv( paths.deaths_path )

    df_c = preprocess_dataframe( df_c, column_names )
    df_d = preprocess_dataframe( df_d, column_names )

    ret = ()

    for name,df,threshold in (('confirmed', df_c, 100),
                              ('deaths',    df_d, 10)):

        # Create series for all countries.

        df_by_country = df.groupby(level=(0,1)).sum()
        m,d,y         = df.columns[-1].split('/')
        last_date     = "%02d%02d%02d" % (int(y), int(m), int(d))
        series        = relative_timeseries( df_by_country, threshold )

        ret += ( PlotDescription( plot_type = name,
                                  name      = 'overall',
                                  last_date = last_date,
                                  series    = series,
                                  threshold = threshold ), )

        for region in df_by_country.index.levels[0]:

            # Filter by region.

            df_region = df_by_country.loc[region]

            if region == 'unknown':
                print( "Territories not matched to WHO region: ",
                       ", ".join(list(df_region.index)) )
                print( df_region )
                continue

            series_filtered = { name:vals for name,vals in series.items()
                                if name[0] == region }

            ret += ( PlotDescription( plot_type = name,
                                      name      = region,
                                      last_date = last_date,
                                      series    = series_filtered,
                                      threshold = threshold ), )

    return ret

def describe_province_plots( paths: DataFilePaths,
                             column_names: ColumnNames,
                             subname: str ):

    if not subname:
        raise RuntimeError( 'make_province_graphs() requires non-empty subname' )

    df_c = pd.read_csv( paths.confirmed_path )
    df_d = pd.read_csv( paths.deaths_path )

    df_c = df_c.set_index( [column_names.country_column, column_names.province_column] )
    df_d = df_d.set_index( [column_names.country_column, column_names.province_column] )

    ret = ()

    for name,df,threshold in (('confirmed', df_c, 100),
                              ('deaths',    df_d, 10)):

        df_by_province = df.groupby(level=(0,1)).sum()

        m,d,y         = df.columns[-1].split('/')
        last_date     = "%02d%02d%02d" % (int(y), int(m), int(d))
        series        = relative_timeseries( df_by_province, threshold )

        ret += ( PlotDescription( plot_type = name,
                                  name      = subname,
                                  last_date = last_date,
                                  series    = series,
                                  threshold = threshold ), )

    return ret
            
def preprocess_dataframe( dataframe: pd.DataFrame,
                          column_names: ColumnNames,
                          province_whitelist: dict = None ):

    ret = dataframe.copy()
    ret[column_names.region_column] = who_region_series( dataframe[ column_names.country_column ],
                                                         dataframe[ column_names.province_column ] )

    ret = ret.set_index( list(column_names) )

    if province_whitelist:

        to_drop = []

        for row in ret.index:
            if row[1] in province_whitelist and row[2] not in province_whitelist[ row[1] ]:
                to_drop.append( row )

        ret = ret.drop( to_drop )

    return ret

def who_region( country, province=None ):
    for r,cs in WORLD_REGIONS.items():
        if province is not None and province in cs:
            return r
        if country in cs:
            return r
    return 'unknown'

def who_region_series( country_series, province_series=None ):

    if province_series is not None:

        return pd.Series( [ who_region( country_series[idx], province_series[idx] )
                            for idx in country_series.index ],
                          index = country_series.index )
    else:

        return pd.Series( [ who_region( country_series[idx] )
                            for idx in country_series.index ],
                          index = country_series.index )

def relative_timeseries( dataframe: pd.DataFrame,
                         threshold: int ):

    df = dataframe.filter( regex='\d+/\d+/\d+' )
    ret = {}

    for idx in df.index:

        row = df.loc[idx]
        above_threshold = [c for c,v in enumerate(row) if v >= threshold]

        if above_threshold:
            min_col = min(above_threshold)
            ret[ idx if type(idx) == str else tuple(idx) ] = row[min_col:]

    return ret

def plot_many( descs: typing.Sequence[ PlotDescription ] ):

    # Ensure output directory exists.

    try:
        os.mkdir( PLOT_DIR )
    except FileExistsError:
        pass

    # Group descs by type (confirmed vs. deaths)

    by_type = { t:() for t in set([desc.plot_type for desc in descs]) }

    for desc in descs:

        by_type[ desc.plot_type ] += (desc,)

    # Plot all series of the same time with the same scale.    

    for plot_type, descs in by_type.items():
        
        max_len = max( [ tc_max_len( desc.series ) for desc in descs ] )
        max_val = max( [ tc_max_val( desc.series ) for desc in descs ] )

        for desc in descs:

            title = "%s, %s, %s" % (desc.plot_type, desc.name, desc.last_date)
            filename = plot_timeseries( desc.series,
                                        title = title,
                                        filename = os.path.join( PLOT_DIR, to_filename(title) + '.png' ),
                                        xlim = (0,max_len),
                                        ylim = (desc.threshold, max_val * 2),
                                        ylabel = desc.plot_type )

            print( 'Wrote %s' % filename )

            filename_latest = os.path.join( PLOT_DIR,
                                            to_filename('%s_%s_latest.png' % (desc.plot_type, desc.name) ) )
            shutil.copy( filename, filename_latest )
            print( 'Wrote %s' % filename_latest )

def display_name( s: typing.Union[ str, typing.Sequence[str] ] ) -> str:
    return s if isinstance(s, str) else s[-1]
            
def plot_timeseries( series: dict,
                     title: str,
                     filename: str,
                     xlim: int, ylim: int,
                     ylabel: str ):

    # borrows logic from this tutorial for legend:
    # https://matplotlib.org/gallery/lines_bars_and_markers/markevery_prop_cycle.html

    maxs = sorted( [ (name,max(vals)) for name,vals in series.items() ],
                   key = lambda tpl: tpl[1],
                   reverse=True )

    to_show = [ name for name,max_val in maxs[:20] ]

    print( "Max values plotted:  "
           + ', '.join( ['%s:%d'%(name,max_val)
                         for name,max_val in maxs[:20]] ) )

    cmap = plt.get_cmap('tab20')
    cmap_idx_scale = math.floor( len(cmap.colors) / 20 )
    
    fig = plt.figure()
    ax = fig.add_axes([0.1, 0.1, 0.6, 0.75], yscale='log')

    for i,name in enumerate(to_show):
        vals = series[name]
        ax.plot( list(range(len(vals))), vals,
                 marker=',',
                 linestyle='solid' if i < 10 else 'dashed',
                 label=display_name(name),
                 color = cmap.colors[i * cmap_idx_scale] )

    ax.set_xlim(xlim)
    ax.set_ylim(ylim)
    ax.set_xlabel( 'days since reaching threshold' )
    ax.set_ylabel( ylabel )
    ax.set_title( title + ", highest 20" )
    ax.legend(bbox_to_anchor=(1.05, 1.0), loc='upper left', borderaxespad=0,
              fontsize='small')
    fig.savefig( filename )
    return filename

def do_action( action ):

    if action == 'download':

        for paths in (DEFAULT_GLOBAL_FILE_PATHS,
                      DEFAULT_US_FILE_PATHS):
            local_paths = download( archive_url = DEFAULT_ARCHIVE_URL,
                                    paths       = paths )
            print( "Downloaded %s" % local_paths.confirmed_path )
            print( "Downloaded %s" % local_paths.deaths_path )

    elif action == 'plot':

        global_plot_descs = describe_country_plots( paths = DEFAULT_GLOBAL_FILE_PATHS,
                                                    column_names = DEFAULT_GLOBAL_COLUMN_NAMES )
        us_plot_descs = describe_province_plots( paths   = DEFAULT_US_FILE_PATHS,
                                                 column_names = DEFAULT_US_COLUMN_NAMES,
                                                 subname = 'US states' )
        plot_many( global_plot_descs + us_plot_descs )

    else:

        print('Unknown action "%s"' % action )

def main( action ):

    if action == 'all':
        do_action( 'download' )
        do_action( 'plot' )
    else:
        do_action( action )
        
if __name__ == '__main__':
    import plac
    plac.call( main )
