
# Covid-19 visualized

## What is this?

This repo attempts to replicate the log-scale plots of the Covid-19
pandemic as seen in the Financial Times and other sources.  The plots
show each locale's trajectory starting from when a threshold value is
reached (100 confirmed cases or 10 deaths).  Each plot contains up to
20 locales, taking those whose latest tally is highest.

The dataset of locales, their labels, and their timelines of confirmed
cases and deaths are sourced from the [Johns Hopkins data
repo](https://github.com/CSSEGISandData/COVID-19).  Regional groupings
are sourced from the [WHO website](https://who.int), particularly
their [Covid-19 situation
reports](https://www.who.int/emergencies/diseases/novel-coronavirus-2019/situation-reports).
The author makes no claim regarding the structure, completeness, or
accuracy of the source data.

Currently, this repo generates plots for:

 * the world, by 'Country/Region' in the JH dataset

 * each WHO region, by 'Country/Region' in the JH dataset.  Note that
   due to overseas territories, some entries may appear in multiple
   WHO regions.

 * the United States, by state

To run the script:

    # Downloads latest data and plots it
    python make_plots.py all

    # Downloads only
    python make_plots.py download

    # Plots only (using previously downloaded data)
    python make_plots.py plot

The script requires the following python modules:

 * [matplotlib](https://matplotlib.org)

 * [pandas](https://pandas.pydata.org)

 * [plac](https://micheles.github.io/plac/)

 * [requests](https://requests.readthedocs.io)

## Confirmed cases (global)

![Confirmed cases, overall](plots/confirmed_overall_latest.png "Confirmed cases, overall")

![Confirmed cases, Europe](plots/confirmed_Europe_latest.png "Confirmed cases, Europe")

![Confirmed cases, Western Pacific](plots/confirmed_Western_Pacific_latest.png "Confirmed cases, Western Pacific")

![Confirmed cases, Eastern Mediterranean](plots/confirmed_Eastern_Mediterranean_latest.png "Confirmed cases, Eastern Mediterranean")

![Confirmed cases, Americas](plots/confirmed_Americas_latest.png "Confirmed cases, Americas")

![Confirmed cases, Africa](plots/confirmed_Africa_latest.png "Confirmed cases, Africa")

![Confirmed cases, Southeast Asia](plots/confirmed_Southeast_Asia_latest.png "Confirmed cases, Southeast Asia")

## Deaths (global)

![Deaths, overall](plots/deaths_overall_latest.png "Deaths, overall")

![Deaths, Europe](plots/deaths_Europe_latest.png "Deaths, Europe")

![Deaths, Western Pacific](plots/deaths_Western_Pacific_latest.png "Deaths, Western Pacific")

![Deaths, Eastern Mediterranean](plots/deaths_Eastern_Mediterranean_latest.png "Deaths, Eastern Mediterranean")

![Deaths, Americas](plots/deaths_Americas_latest.png "Deaths, Americas")

![Deaths, Africa](plots/deaths_Africa_latest.png "Deaths, Africa")

![Deaths, Southeast Asia](plots/deaths_Southeast_Asia_latest.png "Deaths, Southeast Asia")

## Confirmed cases (US states)

![Confirmed cases, US states](plots/confirmed_US_states_latest.png "Confirmed cases, US states")

## Deaths (US states)

![Deaths, US states](plots/deaths_US_states_latest.png "Deaths, US states")